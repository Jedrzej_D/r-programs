### Beta version
###
# Bias correction of Random Forest with examples (at the end)
# used 'Improvements to random forest methodology' Ruo Xu
# Example of usage:
# bRF=bias_RF(x=women,y='height',it=5,seed=8,plot=T)

bias_RF = function(x,y,it=2,ntree=200,seed=FALSE,plot=FALSE,nodes=2){
  require(dplyr)
  require(randomForest)
  rf_list=list() #set output list
  bias_cor_MSE = rep(0,it)
  y_name=y
  y = x[,colnames(x)==y]
  x = dplyr::select(x, -(y_name))
  if (seed==1) {set.seed(seed)}
  rf = randomForest(y ~., data=x,ntree=ntree) #initial forest
  rf_list[[1]]=rf
  Y0=rf$predicted
  bias = Y0-y #initial bias
  
  #loop for next corrections
  for (i in 1:it){
      df = data.frame(bias,x)
      if (seed==1) set.seed(seed)
      rf2 = randomForest(bias~.,df,maxnodes=nodes)
      B0 = rf2$predicted
      rf2$predicted = NULL
      Y0 = Y0 - B0
      bias_cor_MSE[i] = sum((Y0-y)^2)/length(y)
      bias =  (Y0 - y)
      rf_list[[i+1]]=rf2
  }
  
  original_MSE=sum((rf$predicted-y)^2)/length(y)
  v=list(original_MSE,bias_cor_MSE,Y0,rf_list)
  names(v)=c('original_MSE','MSE_corrections','final_vars','rf_list')
  if (plot) matplot(cbind(y,Y0,rf$predicted),cex=0.8,ylab = "values",xlab="rows", main="Plot of real values, last corrected, first predicted")
  print('Finished :)')
  return(v)
  
}

# function for predicting based on a new dataset
# Example of usage: bias_RF_predict(xtest=iris[35:50,],ytest=y,bRF=bRF,it=4)
bias_RF_predict = function(xtest,ytest,bRF,it=3){
  #if (it<2) {print('Warning: Required it>=2, setting to 2') 
  #          it=2}
  require(randomForest)
  corr=0
  if (it>1) corr=rowSums(sapply(2:it, function(i) predict(bRF$rf_list[[i]],xtest))) #sum of corrections
  
  y_cor = predict(bRF$rf_list[[1]],xtest) - corr
  MSE_cor = sum((y_cor-ytest)^2)/length(as.vector(ytest))
  MSE = sum((predict(bRF$rf_list[[1]],xtest)-ytest)^2)/length(as.vector(ytest)) #MSE of original 1st forest
  ls=list(y_cor,MSE_cor,MSE)
  names(ls)=c('y_corrected','MSE_corr','MSE')
  return(ls)
}
############################# End of functions

library(MASS)

MSE_corrc=0
bRFc=0
for (o in 1:10){
y = swiss[35:47,1]
bRF=bias_RF(x=swiss[1:35,],y='Fertility',it=10)
MSE_corr=rep(0,9)
for (k in 1:10){
  MSE_corr[k]=bias_RF_predict(xtest=swiss[35:47,-1],ytest=y,bRF=bRF,it=k)$MSE_corr
}
MSE_corrc=MSE_corr+MSE_corrc
bRFc=bRF$MSE_corrections+bRFc
}
MSE_corrc=MSE_corrc/10
bRFc=bRFc/10
par(mfrow=c(1,2))
plot(bRFc,col='green',pch=16,main='Train set',ylab='MSE',xlab='Iteration',type='o')
plot(MSE_corrc,col='blue',pch=16,main='Test set',ylab='MSE',xlab='Iteration',type='o')

y=iris[35:50,1]
bRF=bias_RF(x=iris[1:34,],y='Sepal.Length',it=10,plot=T)
MSE_corr=rep(0,9)
for (k in 1:10){
  MSE_corr[k-1]=bias_RF_predict(xtest=iris[35:50,],ytest=y,bRF=bRF,it=k)$MSE_corr
}
par(mfrow=c(1,2))
plot(bRF$MSE_corrections,col='green',pch=16,main='Train set',ylab='MSE',xlab='Iteration',type='o')
plot(MSE_corr,col='blue',pch=16,main='Test set',ylab='MSE',xlab='Iteration',type='o')

y=iris[35:50,2]
bRF=bias_RF(x=iris[1:34,],y='Sepal.Width',it=10,plot=T)
MSE_corr=rep(0,9)
for (k in 1:10){
  MSE_corr[k-1]=bias_RF_predict(xtest=iris[35:50,],ytest=y,bRF=bRF,it=k)$MSE_corr
}
par(mfrow=c(1,2))
plot(bRF$MSE_corrections,col='green',pch=16,main='Train set',ylab='MSE',xlab='Iteration',type='o')
plot(MSE_corr,col='blue',pch=16,main='Test set',ylab='MSE',xlab='Iteration',type='o')


MSE_corrc=0
bRFc=0
for (o in 1:10){
  y=Boston[401:506,14]
  bRF=bias_RF(x=Boston[1:400,],y='medv',it=10)
  MSE_corr=rep(0,9)
  for (k in 1:10){
    MSE_corr[k-1]=bias_RF_predict(xtest=Boston[401:506,],ytest=y,bRF=bRF,it=k)$MSE_corr
  }
  MSE_corrc=MSE_corr+MSE_corrc
  bRFc=bRF$MSE_corrections+bRFc
}
MSE_corrc=MSE_corrc/10
bRFc=bRFc/10
par(mfrow=c(1,2))
plot(bRFc,col='green',pch=16,main='Train set',ylab='MSE',xlab='Iteration',type='o')
plot(MSE_corrc,col='blue',pch=16,main='Test set',ylab='MSE',xlab='Iteration',type='o')

library(ISLR)
y=Credit[351:400,12]
bRF=bias_RF(x=Credit[1:350,],y='Balance',it=10,plot=T)
MSE_corr=rep(0,9)
for (k in 1:10){
  MSE_corr[k-1]=bias_RF_predict(xtest=Credit[351:400,],ytest=y,bRF=bRF,it=k)$MSE_corr
}
par(mfrow=c(1,2))
plot(bRF$MSE_corrections,col='green',pch=16,main='Train set',ylab='MSE',xlab='Iteration',type='o')
plot(MSE_corr,col='blue',pch=16,main='Test set',ylab='MSE',xlab='Iteration',type='o')

library(mlbench)
data(Servo)
y=Servo[131:167,5]
bRF=bias_RF(x=Servo[1:130,],y='Class',it=8)
MSE_corr=rep(0,8)
for (k in 1:9){
  MSE_corr[k-1]=bias_RF_predict(xtest=Servo[131:167,],ytest=y,bRF=bRF,it=k)$MSE_corr
}
par(mfrow=c(1,2))
plot(bRF$MSE_corrections,col='green',pch=16,main='Train set',ylab='MSE',xlab='Iteration',type='o')
plot(MSE_corr,col='blue',pch=16,main='Test set',ylab='MSE',xlab='Iteration',type='o')

